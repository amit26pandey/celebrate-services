package com.wwt.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CelebrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CelebrateApplication.class, args);
	}

}
