package com.wwt.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wwt.demo.model.Messages;
import com.wwt.demo.repository.MessageRepository;

@RestController
@RequestMapping("/api/auth")
public class MessageController {
	
	@Autowired
	private MessageRepository messageRepository;
	
	
	 @CrossOrigin
	 @PostMapping("/message")
	 public Messages createMessage(@Validated @RequestBody Messages message) {
		 Messages message1 = messageRepository.save(message);
		 
		 return message1;
	 }
	 
	 @CrossOrigin
	 @GetMapping("/message")
	 public Page<Messages> listMessage(Pageable pageable) {
		 return messageRepository.findAll(pageable);
	 }

}
