package com.wwt.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.wwt.demo.exception.ResourceNotFoundException;
import com.wwt.demo.model.Users;
import com.wwt.demo.repository.UserRepository;

@RestController
@RequestMapping("/api/auth")
public class UserController {
	
	 @Autowired
	 private UserRepository userRepository;
	 
	 @CrossOrigin
	 @PostMapping("/user")
	 public Users createUser(@Validated @RequestBody Users user) {
		 Users user1 = userRepository.save(user);
		 
		 return user1;
	 }
	 
	 @CrossOrigin
	 @GetMapping("/user")
	 public Page<Users> listUser(Pageable pageable) {
		 return userRepository.findAll(pageable);
	 }
	 
	 @CrossOrigin
	 @GetMapping("/users/{id}")
	 public Users getById(@PathVariable Long id) {
		 Users user = userRepository.getOne(id);
		 return user;
		 
	 }
	 
	 @CrossOrigin
	 @DeleteMapping("/user/{id}")
	 public ResponseEntity<?> deleteUser(@PathVariable Long id) {
		 return userRepository.findById(id)
				 .map(user -> {
					 userRepository.delete(user);
					 return ResponseEntity.ok().build();
				 }).orElseThrow(() -> new ResourceNotFoundException("User not found with the id " + id));
		 
	 }
	 
//	 @CrossOrigin
//	 @GetMapping("/login")
//	 public List<Users> loginUser(@RequestParam(value="email") String email,@RequestParam(value="password") String password) {
//		 List<Users>  user = userRepository.findByEmailAndPassword(email,password);
//		 return user;
//	 }
}
