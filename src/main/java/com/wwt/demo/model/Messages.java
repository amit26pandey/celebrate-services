package com.wwt.demo.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "messages")
@JsonIgnoreProperties({"hibernateLasyIniializer","handler"})
public class Messages {
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedFrom() {
		return createdFrom;
	}
	public void setCreatedFrom(String createdFrom) {
		this.createdFrom = createdFrom;
	}
	public String getCreatedTo() {
		return createdTo;
	}
	public void setCreatedTo(String createdTo) {
		this.createdTo = createdTo;
	}
	
	
	@Id
	@Column(name = "id",nullable = false)
	@SequenceGenerator(name = "users_seq", sequenceName = "users_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "users_seq")
	private long id;
	private String message;
	private Date createdDate = new Date();
	private String createdFrom;
	private String createdTo;
}
