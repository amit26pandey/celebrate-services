package com.wwt.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wwt.demo.model.Users;

//import antlr.collections.List;

public interface UserRepository extends JpaRepository<Users,Long> {
	Optional<Users> findByUsername(String username);
	
	Optional<Users> findByEmail(String email);
	
	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
