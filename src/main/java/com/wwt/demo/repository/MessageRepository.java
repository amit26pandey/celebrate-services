package com.wwt.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wwt.demo.model.Messages;

public interface MessageRepository extends JpaRepository<Messages,Long> {

}
